# Nextflow tutorial

Nextflow was conceived for bioinformaticians in the first place, but can be used 
more broadly in ML systems or other, as an easy interface between the task manager and your pipelines.
Nextflow (as far as I know) allows you to :

 * submit a complex pipeline, each step needing different cpu/gpu ressources, with a few lines of code.
 * resume your computations very easily if the server gets down or if you want to free some space for other users etc...
 * handle your output folder tree structure with ease.

You can follow the [nextflow installation instruction](https://www.nextflow.io/docs/latest/getstarted.html). 
This tutorial is just a summary of how I am using nextflow. It does not have the ambition to replace the exhaustive and well written 
[official nextflow documentation](https://www.nextflow.io/docs/latest/basic.html).

## Table of content

1. Basic components of a pipeline : 
 [Channels](#channels) 
◦ [Processes](#process) 
2. Building a pipeline : 
  [Linking processes](#link)
 ◦ [Channel operators](#operators)
 ◦ [Visualisation](#illustration)
3. Set the headers :
  [Computing ressources](#comput)
 ◦ [Writing outputs](#outputs)
4. Debugging, stopping, resuming jobs: 
  [Debugging](#debugs)
 ◦ [Resuming](#resume)
5. Good practices :
  [Submitting a nextflow pipeline](#submitting)
 ◦ [Cleaning the work folder](#cleaning)
 ◦ [Organize your experiments](#organize)

## Basic components of a pipeline <a name="basic"></a>

The basic component of a nextflow pipeline is a nextflow process.
Each process will take an **input** channel and output an ... **ouptut** channel !
A process will have to submit jobs in function of its input items.
These inputs and outputs are organized into channels; a good understanding of their behaviours is the only tricky
part when building a pipeline.

### Nextflow channels <a name="channels"></a>

A channel is simply a list of objects that can be fed to a process. 
A process will then *read* the elements of a channel one by one, until the channel is empty. 

> **Once a channel has been read, it no longer contains elements.**

A corrolary of that is that if you need to execute two different process on the same input,
then you will have to duplicate your input channel to feed these processes.

The elements of a channel can be of different types, the one I use are either :

* **val** : it is just the value we have put in the channel in the begining. Can be a float, an int or a string.
* **file** : a file object is useful, many of its attribute are easily reachable (baseName, full_path).

The type of an input is declared in the `input` block of a process.

I frequently want to create a channel by filling it with all the files that are contained in a folder (input images for example).
To do so, you can use a channel method such as `fromPath`, in the same way you could use `glob` in python.
for example, if in `/path/my_folder` I want to create a channel containing all the `.png` files in it, I will :

```
input_images = Channel.fromPath('/path/my_folder/*.png')
```

Once again you can find an exhaustive list of all the channel methods availables on [the nextflow documentation page](https://www.nextflow.io/docs/latest/channel.html#frompath)

### Nextflow process <a name="process"></a>

Here is one simple example:

<div><img src="figures/nextflow_process.png"></div>

There is four components in a process :

1. **header** : The first line in the definition of the process will **set computing and writing parameters**. 
You will there be able to set the writing folders. If it does not exist, it will be created, 
if it exists, conflicting files will be overwritten. It is possible to write different files in different
 output folders by specifying a `pattern` (that can include regular expressions).
I also set cpus to 5 : each one of the jobs that will be launched by this process will request 5 cpus.
2. **input** : will there define the ... inputs. The way they will be unpacked by the process.
Here, we supposed that the input channel is a collection of tuple (input_file, param). **As long as there
is tuples inside the `ìnput_channel`, the `script`is ran.** Once the channel is empty, the process ends.
3. **output** : define the channels that this process will fill. This channels could then be used as input
channel for downstream processes. In fact the power of nextflow is to chain different processes...
4. **script** : we first can script using nextflow scripting capabilities (I here define an output folder that will depend on the input).
then what is between **`"""`** will be executed as a bash script (do not hesitate to put more than one line if you need to).

## Building a pipeline <a name="build"></a>

### How to link processes <a name="link"></a>

There is no need to specify the link between different processes. It is an implicit link, drawn from the Channels that 
are present in the script and defined as input or outputs of processes.

When a nextflow script is executed, then all the processes present in it are **waiting** for their input channel(s) to be filled !
That means that for a process to start, its input channel needs to contain at least one item.

### Channel operators <a name="operators"></a>

In order to create complexe graph-shaped pipelines, you can combine channels thanks to **[Channel operators](https://www.nextflow.io/docs/latest/operator.html)**.

For example, let's suppose that `process_1` and `process_2` respectively outputs `output_1` and `output_2` 
into `output_chan_1` and `output_chan_2`,
and that `process_3` needs as input an elements from both output channels.
Then we just have to create the input channel of the process 3 by using the 'combine' operators :

```
input_process_3 = output_chan_1 .combine { output_chan_2 }
```

The `input_chan_3` will be composed of all the possible pairs of elements from `output_chan_1` and `output_chan_2`.
As previously said, process_3 will only begin when one element of `input_chan_3` is filled, that meaned that as soon as one
process of `process_1` and one of `process_2` are over.

> Just by defining in that way the channels linking the processes,
we can have a lot of different downstream processes running simultaneously !

This is, in my opinion, one of the advantage of nextflow.

### Illustration <a name="illustration"></a>

Here is an animation to illustrate how a simple pipeline may be executed.
In this example, we suppose that `/path/my_ml_trainer.py` takes as input the path of a dataset on which a model will be trained. 
I want to train a different model for each dataset and at each resolution `R`.
This python script is supposed to write a file containing the output model; this file is then used by a second process, `TestResults`, that will test this model
and write a csv file containing the results.

<div><img src="figures/processes_animation.gif" width="200"></div>

If all goes well, I will be able to find both the models and the csv results files in
`~/nextflow_tuto` and `~/nextflow_tuto/results`.

## Header of processes : tips and tricks <a name="header"></a>

### Management of computing ressources <a name="comput"></a>

In the header you can define what ressources the process will ask for.
In fact, each execution of a process will be submitted by nextflow as an independant job, 
therefore asking for particular ressources.

Each request represent a line in the header of the process.

```
[...]
process Test {
executor 'slurm'
queue 'cpu'
memory '40GB'
cpus 6
maxForks 10

[...]
}
```

Thanks to this header, each run of the `Test` process will ask for 40Gb of memory and 6 cpus, 
on the queue (partition) named 'cpu'. The jobs will be submitted thanks to the executor 'slurm'. 
The `maxForks` arguments forbids nextflow from submitting more than 10 jobs simultaneously from this process.

Because some (but not all) of the job instances may require differents memory ressources, you can update them dynamicaly.

```
process Test {
    executor 'slurm'
    queue 'cpu'
    memory {40GB + 5GB * task.attempt}
    errorStrategy 'retry'
    maxRetries 5
[...]
}
```

With this option, if one instance of Test exceeds the memory requirement of 40GB, 
the job will crash. However thanks to `errorStrategy`, the crashed process can be submitted again and the `task.attempt` increased by one, 
therefore asking for 50GB of memory for it. I fixed the number of possible retries to 5.

Finally, if you do not know how is transcribed an option you use when submitting yourself a process (in a `#SBATCH` preambule command line),
just add it as a string after the header option `clusterOptions`:

```
process Test {
    executor 'slurm'
    queue 'gpu-cbio'
    clusterOptions '--gres=gpu:1'
[...]
}
```

Here I am asking for 1 GPU on the partition 'gpu-cbio'.

### Writing outputs <a name="outputs"></a>

The management of the output files is made easy with nextflow.

When a nextflow process submits a job, a working directory is associated to this particular jobs. 
By default, this working directory is located where the nextflow.nf file is executed, under the name `work/id_of_job`.

Therefore when one of your script is asked to write a file in the currend working directory `'./'`, it will be written in this special `work/id_of_job` directory.
That means that at the end of the execution, this directory is filled with intermediary files, but also files that you may want to use later.

The `publishDir` header option asks nextflow to copy (or symlink depending on the mode used) all the files present in `work/id_of_job` into a given directory.

```
process Test {
    publishDir "./outputs/${param}/logs", pattern: "*.log", mode: 'copy'
    publishDir "./outputs/${param}/results", pattern: "*.csv", mode: 'copy'

[...]
}
```

In this example, at the end of an instance of Test and assuming that `param` is `2` (why not?), the files having the `.log` extension will be copied 
into `./outputs/2/logs/`, and the ones having the `.csv` extension will be copied in `./outputs/2/results/`.

> Do not try to manage the writing path of the outputs files inside your python scripts ! Always ask your scripts to write in the cwd `'./'` and 
manage the file destination within the header of your processes.

That way you can easily adapt the output folder in function of the name of the input or any other parameters.

## Debugging, stopping, resuming jobs <a name="logs"></a>

Last but not least, nextflow allows us to stop, resume, and debug easily a pipeline.

### Debugging <a name="debug"></a>

It may happend that in the middle of your pipeline execution, one particular job crashed.
It is possible to investigate and debug the error that caused this crash.
When you run a `.nf` file, a `.nextflow.log` file is automatically created in your current working directory.
It details the execution of the pipeline, and if it crashed, you just have to go to the end of the file where the error is reported.

A `workdir` is specified there, this is the working directory of the job that crashed 
(the one where output files are written in the first place, `work/id_of_job`).

<div><img src="figures/nextflow_log.png"></div>

You can `cd` in it, a group of hidden files are present here (`ls -a` to see them).
The `'.command.out'` contains the standard output of the job, the `'.command.run'` the slurm submitting file created by nextflow, 
and `'.command.sh'` contains the bash script executed by the process.

> It allows you to debug errors that may appear only with very specific input parameters.

To debugg this script you just have to execute this `sh` file
```
bash .command.sh
```

or if you want to debug it in python, open ipython and copy the sh command in the interpreter.

> Consider using an interactive job to debug your scripts !

Otherwise the frontale of the cluster will crash.

### Resuming jobs <a name="resume"></a>

Several reasons may cause the stopping of your jobs: you want to free ressources for other users, 
one of you script crashed, you forgot to change a parameter in an intermediate process...

To do so, in the working directory (where you ran the `.nf` file), you can run the command : 
```
nextflow log
```

It prints information about the last nextflow scripts that have been run from this directory.

<div><img src="figures/nextflow_log_2.png"></div>

Each nextflow run hava a specific 'Session ID' (or equivalently a specific Run Name).
If you want to resume a nextflow session where it stopped, just run :

```
nextflow your_nextflow_script.nf -resume run_name
```

The jobs that were already finished will be cached and the execution will resume.

# Good Practices <a name="good"></a>

## Submitting <a name="submitting"></a>

To submit a nextflow script you just have to:

```
nextflow run my_script.nf
```

It will print an output as this one : 

<div><img src="figures/nextflow_launch.png"></div>

That will show the advancement of the pipeline.
To stop it, you normally just have to `ctrl + C`. 
However I witnessed some problems in the cancelation of jobs when using only that command.
Sometimes, some jobs mays continue running on a node, causing it to drain (and we don't want that).
Make sure to verify with a `squeue` if your jobs have in fact been canceled.
If not, use `scancel` to kill them properly.

 Nextflow is a program that uses computing ressources, just to submit jobs ! Therefore, when using it on a cluster (Thalassa especially),
in order not to clog the frontal computer, 

> you will have to ask for ressources in order to submit your pipeline.

My usual workflow to submit something is :

1. create a screen `screen -S my_job`
2. ask for an interactive job on this screen : `srun --pty -p cpu bash`
3. submit my nextflow script : `nextflow run my_script.nf`
4. quit the screen `ctrl A + D`.
5. if I want to control at what step is my pipeline, just open the screen again `screen -r my_job`.

## Cleaning <a name="cleaning"></a>

Once you pipeline has successfully ended, you may want to delete the `work` directory that nextflow has created.
In fact all the intermediate files are still in it, the working directories of the failed processes are also here, and
this folder can easily grow in size.

Do not do that however if you used the mode symlink in the publishDir option.

## Organizing folders <a name="organize"></a>

In my opinion, the best way to keep a clean view of you past experiments (and being able to resume them easily), is to 
create a separate folder for each experiments, and copy the *mother* `.nf` files in each of them.

Otherwise we can easily get confused between different experiments sharing the same `.nf` file and the same `work` directory, 
making them harder to resume for example. 

<div>
<img src="figures/git_cat.gif">
</div>


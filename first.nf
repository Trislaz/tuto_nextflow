#!/usr/bin/env nextflow

input = Channel.from('first', 'second', 'third')
param = Channel.from(1, 2, 3)
input_channel = input.combine( param ) 

process FirstProcess {
    publishDir "${log_folder}", pattern: "*.log", overwrite: true, mode: 'copy'
    publishDir "${output_folder}", pattern: "*.npy", overwrite: true, mode: 'copy'

    input:
    set file(in), val(p) from input_channel

    output:
    file('*.npy')

    script:
    output_folder = file("~/nextflow_tuto/${p}/outputs/")
    log_folder = file("~/nextflow_tuto/${p}/logs/")
    """
    touch "${in}.npy"
    echo "${p}" > "${in}.log"
    """
}

process TrainClassifier {
    publishDir "${output_folder}", pattern: "*.model", mode: 'copy'

    input:
    set file(F) from input_channel
    each R from resolution_channel

    output:
    file('*.model') into models_channel


    script:
    output_folder = file("~/nextflow_tuto/models/${R}")
    """
    python /path/my_ml_trainer.py --path_input ${F} --resolution $R
    """
}

process TestModels {
    publishDir "${output_folder}", pattern: "*.csv", mode:'copy'

    input:
    set file(M) from model_channel

    output:
    file('*.csv') into results

    script:
    output_folder = file("~/nextflow_tuto/results/${R}")
    """
    python /path/test.py --path_model ${M}
    """
}
